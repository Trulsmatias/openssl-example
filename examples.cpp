#include "crypto.hpp"
#include <iostream>
#include <string>

using namespace std;

int main() {
  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl;

  string pswd = "aaa";
  string key = "";
  string korrekt = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  for (int i = 122; i > 47; i--) {
      cout << pswd << endl;
      // sjekker
      //cout << "sjekker" << "\n";
      key = Crypto::hex(Crypto::pbkdf2(pswd, "Saltet til Ola", 2048));
      if (key.compare(korrekt) == 0) {
          cout << pswd << " er passordet til Ola!" << endl;
          return 0;
      }
      // endrer
      pswd.at(0) = (char) i;
      // sjekker
      cout << pswd << "\n";
      //cout << "sjekker" << "\n";
      key = Crypto::hex(Crypto::pbkdf2(pswd, "Saltet til Ola", 2048, 160));
      if (key.compare(korrekt) == 0) {
          cout << pswd << " er passordet til Ola!" << endl;
          return 0;
      }
      for (int j = 122; j > 47; j--) {
          // endrer
          pswd.at(1) = (char) j;
          // sjekker
          cout << pswd << "\n";
          //cout << "sjekker" << "\n";
          key = Crypto::hex(Crypto::pbkdf2(pswd, "Saltet til Ola", 2048, 160));
          if (key.compare(korrekt) == 0) {
              cout << pswd << " er passordet til Ola!" << endl;
              return 0;
          }
          for (int k = 122; k > 47; k--) {
              // endrer
              pswd.at(2) = (char) k;
              // sjekker
              cout << pswd << "\n";
              //cout << "sjekker" << "\n";
              key = Crypto::hex(Crypto::pbkdf2(pswd, "Saltet til Ola", 2048, 160));
              if (key.compare(korrekt) == 0) {
                  cout << pswd << " er passordet til Ola!" << endl;
                  return 0;
              }
          }
      }
  }
}
